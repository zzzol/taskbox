# Tasklist Zero Client

First, run `yarn install`.

## Starting

```bash
PORT=3003 npm start
```

(Run tasklist server on port 3000).

## Development

### Prettier

This project uses [prettier](https://github.com/jlongster/prettier).

Install the Atom plugin with these settings:
 - "Single Quote"
 - "Trailing Comma"
 - "Bracket Spacing"

Or do the equivalent with your editor.

### Running Storybook

```
npm run storybook
```

Browse to http://localhost:9009

Story files are alongside the components as `X.story.js`.

### Running tests

```
npm test
```

It should be smart and watch and whatnot.

Unit tests are alongside the components as `X.test.js`.

#### Technology used
- Jest for running tests
- Storyshots for automatic snapshot tests from SB stories
- Enzyme for unit testing component behavior

### Changing CSS

Right now the LESS files are a bit of a mess between the Blaze/React versions of Todos and some minor custom styling for the todo items.

There's no LESS pipeline built in; to re-compile the less, run

```
npm run less
```

The app should hot-reload.
