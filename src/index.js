import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter } from 'react-router-dom';
import { withState } from 'recompose';

import App from './pages/App';
import './index.css';
import {
  login,
  logout,
  attachServiceToken,
  detachServiceToken,
  token
} from './utils/authentication';

const { REACT_APP_API_HOST = 'http://localhost:3000' } = process.env;

const networkInterface = createNetworkInterface({
  uri: `${REACT_APP_API_HOST}/graphql`
});

networkInterface.use([
  {
    applyMiddleware(req, next) {
      req.options.headers = {
        authorization: token ? `JWT ${token}` : null,
        ...req.options.headers
      };
      next();
    }
  }
]);

const client = new ApolloClient({
  networkInterface,
  dataIdFromObject({ id, __typename }) {
    return `${__typename}-${id}`;
  },
  addTypename: true
});

// XXX: hack to get a reference to the client inside reducers.
// We could either (a) refactor so client in more global, or maybe
// (b) use the new update function in AC 0.10
window.client = client;

function onLinkService(service, remove = false) {
  if (remove) {
    detachServiceToken(client, service);
  } else {
    window.open(`${REACT_APP_API_HOST}/connect/${service}`, '_new');
  }
}

const ClientApp = ({ loggedIn, setLoggedIn }) => (
  <ApolloProvider client={client}>
    <BrowserRouter>
      <App
        loggedIn={loggedIn}
        onLogin={(email, password) =>
          login(REACT_APP_API_HOST, email, password).then(() => {
            setLoggedIn(true);
            client.resetStore();
          })}
        onLogout={() =>
          logout().then(() => {
            setLoggedIn(false);
            client.resetStore();
          })}
        onLinkService={onLinkService}
      />
    </BrowserRouter>
  </ApolloProvider>
);
const ClientAppWithLogin = withState('loggedIn', 'setLoggedIn', !!token)(
  ClientApp
);

ReactDOM.render(<ClientAppWithLogin />, document.getElementById('root'));

function handleMessage({ origin, data }) {
  if (origin !== REACT_APP_API_HOST) {
    // Funny/buggers
    return;
  }

  const { message, ...rest } = data;
  if (message === 'attachToken') {
    const { service, token } = rest;
    attachServiceToken(client, service, token);
  }
}

window.addEventListener('message', handleMessage, false);
