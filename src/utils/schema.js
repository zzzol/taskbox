export const TASK_STATES = {
  inbox: 'TASK_INBOX',
  snoozed: 'TASK_SNOOZED',
  pinned: 'TASK_PINNED',
  archived: 'TASK_ARCHIVED',
};
