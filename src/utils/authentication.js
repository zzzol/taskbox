import gql from 'graphql-tag';

const KEY = 'authToken';

export let token = localStorage.getItem(KEY);

export async function login(serverUrl, email, password) {
  const response = await fetch(`${serverUrl}/login`, {
    method: 'POST',
    body: JSON.stringify({ email, password }),
    headers: { 'Content-Type': 'application/json' },
  });
  const data = await response.json();
  token = data.token;
  localStorage.setItem(KEY, token);
}

export async function logout(serverUrl) {
  token = null;
  localStorage.removeItem(KEY);
}

export async function attachServiceToken(client, service, token) {
  return client.mutate({
    mutation: (
      gql`mutation {
      updateUser(input: {
        ${service}Token: "${token}"
      }) {
        id
      }
    }`
    ),
    refetchQueries: ['MenuQuery'],
  });
}

export async function detachServiceToken(client, serviceName) {
  return client.mutate({
    mutation: (
      gql`mutation($serviceName: String!) {
      detachService(serviceName: $serviceName) {
        id
        serviceNames
      }
    }`
    ),
    variables: { serviceName },
    refetchQueries: ['MenuQuery'],
  });
}
