import { MockedProvider } from 'react-apollo/lib/test-utils';
import moment from 'moment';

import { TASK_STATES } from './schema';

export function buildTask(attributes) {
  return {
    id: Math.round(Math.random() * 1000000).toString(),
    title: 'Test Task',
    subtitle: 'on Test Board',
    url: 'http://test.url',
    subtitle_url: 'http://test2.url',
    state: TASK_STATES.inbox,
    snoozeUntil: (
      attributes.state === TASK_STATES.snoozed &&
        moment().add(1, 'day').valueOf()
    ),
    serviceName: (!('url' in attributes) || attributes.url) && 'github',
    updatedAt: Date.now(),
    __typename: 'Task',
    ...attributes,
  };
}

export { MockedProvider };
