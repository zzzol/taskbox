import update from './update';

describe('$addToSetByIdOrdered', () => {
  const sort = 'a';

  it('inserts into an empty set', () => {
    const set = [];
    const item = { id: 2, a: 1 };
    const newSet = update(set, { $addToSetByIdOrdered: { item, sort } });
    expect(newSet.length).toEqual(1);
    expect(newSet[0]).toEqual(item);
  });

  it('inserts into a set at the beginning', () => {
    const set = [ { id: 0, a: 2 }, { id: 1, a: 3 } ];
    const item = { id: 2, a: 1 };
    const newSet = update(set, { $addToSetByIdOrdered: { item, sort } });
    expect(newSet.length).toEqual(3);
    expect(newSet[0]).toEqual(item);
  });

  it('inserts into a set at the end', () => {
    const set = [ { id: 0, a: 1 }, { id: 1, a: 2 } ];
    const item = { id: 2, a: 3 };
    const newSet = update(set, { $addToSetByIdOrdered: { item, sort } });
    expect(newSet.length).toEqual(3);
    expect(newSet[2]).toEqual(item);
  });

  it('inserts into a set in between', () => {
    const set = [ { id: 0, a: 1 }, { id: 1, a: 3 } ];
    const item = { id: 2, a: 2 };
    const newSet = update(set, { $addToSetByIdOrdered: { item, sort } });
    expect(newSet.length).toEqual(3);
    expect(newSet[1]).toEqual(item);
  });

  it('inserts into a set at the same value', () => {
    const set = [ { id: 0, a: 1 }, { id: 1, a: 2 } ];
    const item = { id: 2, a: 2 };
    const newSet = update(set, { $addToSetByIdOrdered: { item, sort } });
    expect(newSet.length).toEqual(3);
    expect(newSet[1]).toEqual(item);
  });

  it('does not insert into a set at the same value if ids are the same', () => {
    const set = [ { id: 0, a: 1 }, { id: 1, a: 2 } ];
    const item = { id: 1, a: 2 };
    const newSet = update(set, { $addToSetByIdOrdered: { item, sort } });
    expect(newSet.length).toEqual(2);
  });

  it('changes position of an item if its sorted value has changed ', () => {
    const set = [ { id: 0, a: 2 }, { id: 1, a: 3 } ];
    const item = { id: 1, a: 0 };
    const newSet = update(set, { $addToSetByIdOrdered: { item, sort } });
    expect(newSet.length).toEqual(2);
    expect(newSet[0]).toEqual(item);
  });
});

describe('$removeFromSetById', () => {
  it('removes an item from a set when the exact same item is in there', () => {
    const set = [ { id: 0 }, { id: 1 } ];
    const item = { id: 1 };
    const newSet = update(set, { $removeFromSetById: item });
    expect(newSet.length).toEqual(1);
  });

  it(
    'removes an item from a set when an item with the same id is in there',
    () => {
      const set = [ { id: 0, a: 4 }, { id: 1, a: 3 } ];
      const item = { id: 1, a: 2 };
      const newSet = update(set, { $removeFromSetById: item });
      expect(newSet.length).toEqual(1);
    },
  );

  it('does nothing when item is not in the set', () => {
    const set = [ { id: 0, a: 4 }, { id: 1, a: 3 } ];
    const item = { id: 3, a: 4 };
    const newSet = update(set, { $removeFromSetById: item });
    expect(newSet.length).toEqual(2);
  });

  it('does nothing when item is not in the set, and the set is empty', () => {
    const set = [];
    const item = { id: 3, a: 4 };
    const newSet = update(set, { $removeFromSetById: item });
    expect(newSet.length).toEqual(0);
  });
});
