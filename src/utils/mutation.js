import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import moment from 'moment';
import { throttle } from 'lodash';
import { compose } from 'recompose';

import update from './update';
import TaskList from '../components/TaskList';
import { TASK_STATES } from './schema';

export const withOnSnoozeTask = graphql(
  gql`
  mutation OnSnoozeTaskMutation($taskId: ObjID!, $state: TaskState!, $snoozeUntil: Float) {
    updateTask(id: $taskId, input: {
      state: $state
      snoozeUntil: $snoozeUntil
    }) {
      ...TaskListTaskFragment
    }
  }
  ${TaskList.fragments.task}
`,
  {
    props: ({ mutate }) => ({
      onSnoozeTask: (taskId, days) => {
        const state = days === 0 ? TASK_STATES.inbox : TASK_STATES.snoozed;
        let snoozeUntil = null;

        if (days > 0 && days !== Infinity) {
          snoozeUntil = moment()
            .add(days, 'days')
            .startOf('day')
            .add(8, 'hours')
            .valueOf();
        }

        return mutate({
          variables: {
            taskId,
            snoozeUntil,
            state,
          },
          optimisticResponse: {
            __typename: 'Mutation',
            updateTask: {
              __typename: 'Task',
              id: taskId,
              state,
              snoozeUntil,
              updatedAt: Date.now(),
            },
          },
        });
      },
    }),
  }
);

export const withOnPinTask = graphql(
  gql`
  mutation OnPinTaskMutation($taskId: ObjID!, $state: TaskState) {
    updateTask(id: $taskId, input: { state: $state }) {
      ...TaskListTaskFragment
    }
  }
  ${TaskList.fragments.task}
`,
  {
    props: ({ mutate }) => ({
      onPinTask: (taskId, pin = true) => {
        const state = pin ? TASK_STATES.pinned : TASK_STATES.inbox;
        return mutate({
          variables: {
            taskId,
            state,
          },
          optimisticResponse: {
            __typename: 'Mutation',
            updateTask: {
              __typename: 'Task',
              id: taskId,
              state,
              updatedAt: Date.now(),
            },
          },
        });
      },
    }),
  }
);

export const withOnArchiveTask = graphql(
  gql`
  mutation OnArchiveTaskMutation($taskId: ObjID!, $state: TaskState) {
    updateTask(id: $taskId, input: { state: $state }) {
      ...TaskListTaskFragment
    }
  }
  ${TaskList.fragments.task}
`,
  {
    props: ({ mutate }) => ({
      onArchiveTask: (taskId, archive = true) => {
        const state = archive ? TASK_STATES.archived : TASK_STATES.inbox;
        mutate({
          variables: {
            taskId,
            state,
          },
          optimisticResponse: {
            __typename: 'Mutation',
            updateTask: {
              __typename: 'Task',
              id: taskId,
              state,
              updatedAt: Date.now(),
            },
          },
        });
      },
    }),
  }
);

export const withOnTaskStateChanges = compose(
  withOnSnoozeTask,
  withOnPinTask,
  withOnArchiveTask
);

export const withOnCreateTask = graphql(
  gql`
  mutation OnCreateTaskMutation($title: String!) {
    createTask(input: { title: $title, state: ${TASK_STATES.inbox} }) {
      ...TaskListTaskFragment
    }
  }
  ${TaskList.fragments.task}
`,
  {
    props: ({ mutate }) => ({
      onCreateTask: title => mutate({
        variables: { title },
        optimisticResponse: {
          __typename: 'Mutation',
          createTask: {
            __typename: 'Task',
            id: Math.random().toString(),
            title,
            state: TASK_STATES.inbox,
            updatedAt: Date.now(),
            subtitle: null,
            subtitle_url: null,
            url: null,
            snoozeUntil: null,
            serviceName: null,
          },
        },
      }),
    }),
  }
);

export const withOnUpdateTaskTitle = graphql(
  gql`
  mutation TaskMutation($id: ObjID!, $title: String!) {
    updateTask(id: $id, input: { title: $title }) {
      ...TaskListTaskFragment
    }
  }
  ${TaskList.fragments.task}
`,
  {
    props({ mutate }) {
      const throttledMutate = throttle(mutate, 300);
      return {
        onUpdateTaskTitle(id, title) {
          return throttledMutate({ variables: { id, title } });
        },
      };
    },
  }
);

export function updateQueryTasklist(
  previousResult,
  action,
  listName,
  ourState
) {
  const changedFields = action.type === 'APOLLO_MUTATION_RESULT' &&
    (action.result.data.updateTask || action.result.data.createTask);

  if (changedFields) {
    if (changedFields.state === ourState) {
      // Read the old value of the task from the store
      const oldTask = window.client.readFragment({
        id: `Task-${changedFields.id}`,
        fragment: TaskList.fragments.task,
        fragmentName: 'TaskListTaskFragment',
      });
      const changedTask = { ...oldTask, ...changedFields };

      // Add the task to our list
      return update(previousResult, {
        me: {
          [listName]: {
            $addToSetByIdOrdered: {
              item: changedTask,
              sort: x => (-1) * x.updatedAt,
            },
          },
        },
      });
    } else {
      return update(previousResult, {
        me: { [listName]: { $removeFromSetById: changedFields } },
      });
    }
  }

  return previousResult;
}
