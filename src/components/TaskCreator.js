import PropTypes from 'prop-types';
import React, { Component } from 'react';

class TaskCreator extends Component {
  static propTypes = { onCreateTask: PropTypes.func.isRequired };

  state = { title: '' };

  focusInput() {
    this.refs.input.focus();
  }

  onInputChange(event) {
    this.setState({ title: event.target.value });
  }

  onSubmit(event) {
    event.preventDefault();

    const { onCreateTask } = this.props;
    const { title } = this.state;

    if (title) {
      return onCreateTask(title).then(() => this.setState({ title: '' }));
    }
  }

  render() {
    const { title } = this.state;

    return (
      <form
        className="js-todo-new todo-new input-symbol"
        onSubmit={e => this.onSubmit(e)}
      >
        <input
          ref="input"
          value={title}
          onChange={e => this.onInputChange(e)}
          type="text"
          placeholder="Add your own task"
        />
        <span
          className="icon-add js-todo-add"
          onClick={() => this.focusInput()}
        />
      </form>
    );
  }
}

export default TaskCreator;
