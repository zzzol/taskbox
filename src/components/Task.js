import React from 'react';
import gql from 'graphql-tag';
import { propType } from 'graphql-anywhere';
import { compose, withState, withHandlers } from 'recompose';
import PropTypes from 'prop-types';
import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap.css';
import moment from 'moment';

import IconLink from './IconLink';
import { TASK_STATES } from '../utils/schema';

export function SnoozeButtons({ id, state, isLinkedTask, onSnoozeTask }) {
  return (
    <div className="snooze-wrapper">
      <div className="tooltip-heading">Snooze until</div>
      <div className="list small">
        <a onClick={() => onSnoozeTask(id, 1)}>
          Tomorrow
        </a>
        <a onClick={() => onSnoozeTask(id, 3)}>
          {moment().add(3, 'days').format('dddd')} (3 days)
        </a>
        {isLinkedTask &&
          <a onClick={() => onSnoozeTask(id, Infinity)}>
            Further activity
          </a>}
        {state === TASK_STATES.snoozed &&
          <a onClick={() => onSnoozeTask(id, 0)}>
            Unsnooze
          </a>}
      </div>
    </div>
  );
}

function SnoozeArea({
  id,
  state,
  isLinkedTask,
  snoozeUntil,
  onSnoozeTask,
  setSnoozeActive
}) {
  const snoozer = (
    <Tooltip
      placement="bottom"
      trigger="click"
      overlay={<SnoozeButtons {...{ id, state, isLinkedTask, onSnoozeTask }} />}
      onVisibleChange={visible => setSnoozeActive(visible)}
    >
      <a className="icon-time" />
    </Tooltip>
  );

  if (state === TASK_STATES.snoozed) {
    if (snoozeUntil) {
      return (
        <span className="time">
          <span className="time-snooze">
            Unsnoozing {moment(snoozeUntil).fromNow()}
          </span>
          {snoozer}
        </span>
      );
    } else {
      return (
        <span className="time">
          <span className="time-snooze">
            Snoozing till further activity
          </span>
          {snoozer}
        </span>
      );
    }
  } else if (state !== TASK_STATES.archived) {
    return snoozer;
  } else {
    return null;
  }
}

function Task({
  editableTitle,
  onTitleChange,
  task: {
    id,
    title,
    url,
    state,
    subtitle,
    subtitle_url,
    snoozeUntil,
    serviceName
  },
  onSnoozeTask,
  onPinTask,
  onArchiveTask,
  snoozeActive,
  setSnoozeActive
}) {
  const isLinkedTask = !!url;

  const serviceImg = `img/favicon-${serviceName}.png`;

  return (
    <div
      className={`list-item ${state} ${snoozeActive ? 'snoozed-button-active' : ''}`}
    >
      <div className="avatar-wrapper">
        <div className="avatar">
          {serviceName
            ? <img
                className="image"
                src={serviceImg}
                title={serviceName}
                alt={serviceName}
              />
            : <div className="initials">TB</div>}
        </div>
        <label className="checkbox" onClick={event => event.stopPropagation()}>
          <input
            type="checkbox"
            defaultChecked={state === TASK_STATES.archived}
            disabled={state === TASK_STATES.archived}
            name="checked"
            onChange={() => onArchiveTask(id, true)}
          />
          <span
            className="checkbox-custom"
            title={state !== TASK_STATES.archived ? 'Mark as done' : ''}
          />
        </label>
      </div>

      <div className="title">
        {isLinkedTask || state === 'TASK_ARCHIVED'
          ? <a href={url} className="text" target="_blank">{title}</a>
          : <input
              type="text"
              value={editableTitle}
              onChange={onTitleChange}
              placeholder="Input title"
            />}
      </div>

      {subtitle &&
        (subtitle_url
          ? <div className="subtitle">
              <a href={subtitle_url} target="_new">{subtitle}</a>
            </div>
          : <p className="subtitle">{subtitle}</p>)}

      <div className="actions" onClick={event => event.stopPropagation()}>
        <SnoozeArea
          {...{
            id,
            state,
            isLinkedTask,
            snoozeUntil,
            onSnoozeTask,
            setSnoozeActive
          }}
        />
        {state !== TASK_STATES.archived &&
          (state === TASK_STATES.pinned
            ? <IconLink
                name="star"
                title="Unstar task"
                className="star"
                onClick={() => onPinTask(id, false)}
              />
            : <IconLink
                name="star-hollow"
                title="Star task"
                className="star"
                onClick={() => onPinTask(id, true)}
              />)}
        {state === TASK_STATES.archived &&
          <a className="reclaim" onClick={() => onArchiveTask(id, false)}>
            Reclaim
          </a>}
      </div>
    </div>
  );
}

Task.fragments = {
  task: gql`
    fragment TaskFragment on Task {
      id
      title
      subtitle
      url
      subtitle_url
      state
      snoozeUntil
      serviceName
    }
  `
};

Task.propTypes = {
  editableTitle: PropTypes.string.isRequired,
  onTitleChange: PropTypes.func.isRequired,
  task: propType(Task.fragments.task).isRequired,
  onSnoozeTask: PropTypes.func,
  onPinTask: PropTypes.func,
  onArchiveTask: PropTypes.func
};

export { Task as PureTask };

const EditableTask = compose(
  withState(
    'editableTitle',
    'setEditableTitle',
    ({ task: { title } }) => title
  ),
  withHandlers({
    onTitleChange: ({
      task: { id },
      onUpdateTaskTitle,
      setEditableTitle
    }) => event => {
      const title = event.target.value;
      setEditableTitle(title);
      onUpdateTaskTitle(id, title);
    }
  }),
  withState('snoozeActive', 'setSnoozeActive', false)
)(Task);

// not including the ones checked by Task
EditableTask.propTypes = {
  onUpdateTaskTitle: PropTypes.func.isRequired
};

EditableTask.fragments = Task.fragments;

export default EditableTask;
