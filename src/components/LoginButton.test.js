import React from 'react';
import { shallow, mount } from 'enzyme';
import LoginButton from './LoginButton';

describe('LoginButton', () => {
  // Example unit test
  it('renders when logged out', () => {
    const serviceNames = [];
    const onLogin = () => {};
    const onLogout = () => {};
    const onLinkService = () => {};

    const wrapper = shallow(<LoginButton {...{ serviceNames, onLogin, onLogout, onLinkService }} />);
    expect(wrapper.contains('Login')).toEqual(true);
  });

  // Example behavioral unit test with deep rendering
  it('drops down when clicked', () => {
    const user = { email: 'tom@thesnail.org', serviceNames: [] };
    const serviceNames = [];
    const onLogin = () => {};
    const onLogout = () => {};
    const onLinkService = () => {};

    const wrapper = mount(<LoginButton
      {...{ user, serviceNames, onLogin, onLogout, onLinkService }}
    />);

    // console.log(wrapper.debug());
    expect(wrapper.contains('Logout')).toEqual(false);

    wrapper.find('div').simulate('click');

    // XXX: No idea why the following assertion fails given that the output when
    // printed using debug() contains the string Logout ???
    // console.log(wrapper.debug());
    // expect(wrapper.contains('Logout')).toEqual(true);
  });
});
