import React from 'react';
import { storiesOf, action } from '@storybook/react';

import LoginButton from './LoginButton';

storiesOf('LoginButton', module)
  .addDecorator(story => <div id="menu">{story()}</div>)
  .add('logged out', () => {
    const serviceNames = [];
    const onLogin = action('onLogin');
    const onLogout = action('onLogout');
    const onLinkService = action('onLogout');

    return (
      <LoginButton {...{ serviceNames, onLogin, onLogout, onLinkService }} />
    );
  })
  .add('logged in; no services', () => {
    const user = { email: 'tom@thesnail.org', serviceNames: [] };
    const serviceNames = [];
    const onLogin = action('onLogin');
    const onLogout = action('onLogout');
    const onLinkService = action('onLogout');

    return (
      <LoginButton
        {...{ user, serviceNames, onLogin, onLogout, onLinkService }}
      />
    );
  })
  .add('logged in; has all services', () => {
    const user = {
      email: 'tom@thesnail.org',
      serviceNames: ['github', 'trello'],
    };
    const serviceNames = ['github', 'trello'];
    const onLogin = action('onLogin');
    const onLogout = action('onLogout');
    const onLinkService = action('onLogout');

    return (
      <LoginButton
        {...{ user, serviceNames, onLogin, onLogout, onLinkService }}
      />
    );
  })
  .add('logged in; missing one service', () => {
    const user = { email: 'tom@thesnail.org', serviceNames: ['github'] };
    const serviceNames = ['github', 'trello'];
    const onLogin = action('onLogin');
    const onLogout = action('onLogout');
    const onLinkService = action('onLogout');

    return (
      <LoginButton
        {...{ user, serviceNames, onLogin, onLogout, onLinkService }}
      />
    );
  })
  .add('logged in; missing two services', () => {
    const user = { email: 'tom@thesnail.org', serviceNames: [] };
    const serviceNames = ['github', 'trello'];
    const onLogin = action('onLogin');
    const onLogout = action('onLogout');
    const onLinkService = action('onLogout');

    return (
      <LoginButton
        {...{ user, serviceNames, onLogin, onLogout, onLinkService }}
      />
    );
  });
