import React from 'react';
import { storiesOf, action } from '@storybook/react';

import TaskCreator from './TaskCreator';

storiesOf('TaskCreator', module).add('basic', () => (
  <TaskCreator onCreateTask={action('onCreateTask')} />
));
