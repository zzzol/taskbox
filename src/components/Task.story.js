import React from 'react';
import { storiesOf, action } from '@storybook/react';

import Task, { SnoozeButtons } from './Task';
import { TASK_STATES } from '../utils/schema';
import { buildTask } from '../utils/test-helpers';

function buildStory(attrs) {
  const task = buildTask(attrs);
  const onPinTask = action('onPinTask');
  const onSnoozeTask = action('onSnoozeTask');
  const onArchiveTask = action('onArchiveTask');
  const onUpdateTaskTitle = action('onUpdateTaskTitle');

  return (
    <Task
      {...{ task, onPinTask, onSnoozeTask, onArchiveTask, onUpdateTaskTitle }}
    />
  );
}

storiesOf('SnoozeButtons')
  .addDecorator(story => <div className="rc-tooltip-inner">{story()}</div>)
  .add('linked unsnoozed task', () => (
    <SnoozeButtons
      id={Math.random()}
      state={TASK_STATES.inbox}
      isLinkedTask={true}
      onSnoozeTask={action('onSnoozeTask')}
    />
  ))
  .add('unlinked unsnoozed task', () => (
    <SnoozeButtons
      id={Math.random()}
      state={TASK_STATES.inbox}
      isLinkedTask={false}
      onSnoozeTask={action('onSnoozeTask')}
    />
  ))
  .add('linked snoozed task', () => (
    <SnoozeButtons
      id={Math.random()}
      state={TASK_STATES.snoozed}
      isLinkedTask={true}
      onSnoozeTask={action('onSnoozeTask')}
    />
  ))
  .add('unlinked snoozed task', () => (
    <SnoozeButtons
      id={Math.random()}
      state={TASK_STATES.snoozed}
      isLinkedTask={false}
      onSnoozeTask={action('onSnoozeTask')}
    />
  ));

storiesOf('Task')
  .addDecorator(story => (
    <div className="list-items" style={{ background: 'white' }}>{story()}</div>
  ))
  .add('inbox task', () => buildStory({ state: TASK_STATES.inbox }))
  .add('inbox task long title', () => buildStory({
    state: TASK_STATES.inbox,
    title: 'This is a very very very very very very long title',
  }))
  .add('inbox task custom', () =>
    buildStory({ state: TASK_STATES.inbox, subtitle: null, url: null }))
  .add('inbox task no subtitle url', () =>
    buildStory({ state: TASK_STATES.inbox, subtitle_url: null }))
  .add('inbox task trello', () =>
    buildStory({ state: TASK_STATES.inbox, serviceName: 'trello' }))
  .add('snoozed task', () => buildStory({ state: TASK_STATES.snoozed }))
  .add('snoozed till further activity', () => buildStory({
    state: TASK_STATES.snoozed,
    snoozeUntil: null,
  }))
  .add('pinned task', () => buildStory({ state: TASK_STATES.pinned }))
  .add('archived task', () => buildStory({ state: TASK_STATES.archived }))
  .add('archived custom task', () => buildStory({
    state: TASK_STATES.archived,
    url: null,
  }));
