import PropTypes from 'prop-types';
import React, { Component } from 'react';
import gql from 'graphql-tag';
import { propType } from 'graphql-anywhere';
import { includes } from 'lodash';

// Foo

class LoginButton extends Component {
  state = { expanded: false };

  toggleExpanded() {
    this.setState({ expanded: !this.state.expanded });
  }

  render() {
    const { user, serviceNames, onLogin, onLogout, onLinkService } = this.props;

    if (user) {
      const { email, serviceNames: userServiceNames } = user;
      const { expanded } = this.state;

      const extraButtons = serviceNames.map(name => {
        const hasService = includes(userServiceNames, name);
        const onClick = e => {
          e.stopPropagation();
          onLinkService(name, hasService);
        };
        return (
          <a key={name} className="btn-secondary" onClick={onClick}>
            {hasService ? 'Remove' : 'Add'} {name}
          </a>
        );
      });

      return (
        <div
          className="btns-group-vertical"
          onClick={this.toggleExpanded.bind(this)}
        >
          <a className="btn-secondary">
            <span className={expanded ? 'icon-arrow-up' : 'icon-arrow-down'} />
            {email}
          </a>
          {expanded &&
            <span className="secondary-wrapper">
              {extraButtons}
              <a
                key="logout"
                className="btn-secondary"
                onClick={() => onLogout()}
              >
                Logout
              </a>
            </span>
          }
        </div>
      );
    } else {
      return (
        <div className="btns-group">
          <a className="btn-secondary" onClick={() => onLogin()}>Login</a>
        </div>
      );
    }
  }
}

LoginButton.fragments = {
  user: (
    gql`
    fragment LoginButtonFragment on User {
      email
      serviceNames
    }
  `
  ),
};

LoginButton.propTypes = {
  user: propType(LoginButton.fragments.user),
  serviceNames: PropTypes.arrayOf(PropTypes.string).isRequired,
  onLogin: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
  onLinkService: PropTypes.func.isRequired,
};

export default LoginButton;
