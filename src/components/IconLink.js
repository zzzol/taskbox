import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap.css';

const IconLink = ({ name, title, ...props }) => {
  const link = (
    <a title={title} {...props}><span className={`icon-${name}`} /></a>
  );
  if (title) {
    return <Tooltip placement="top" overlay={title}>{link}</Tooltip>;
  } else {
    return link;
  }
};

IconLink.propTypes = {
  name: PropTypes.string.isRequired,
  title: PropTypes.string
};

export default IconLink;
