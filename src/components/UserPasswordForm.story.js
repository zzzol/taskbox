import React from 'react';
import { storiesOf, action } from '@storybook/react';

import UserPasswordForm from './UserPasswordForm';
storiesOf('UserPasswordForm', module)
  .addDecorator(story => (
    <div className="page auth">
      <div className="wrapper-auth">
        {story()}
      </div>
    </div>
  ))
  .add('basic', () => (
    <UserPasswordForm submitLabel="Submit" onSubmit={action('onSubmit')} />
  ));
