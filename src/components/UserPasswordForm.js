import PropTypes from 'prop-types';
import React from 'react';
import { withState } from 'recompose';

function UserPasswordForm(
  { submitLabel, onSubmit, email, setEmail, password, setPassword },
) {
  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        onSubmit(email, password);
      }}
    >
      <div className="input-symbol">
        <input
          type="email"
          name="email"
          value={email}
          onChange={e => setEmail(e.target.value)}
          placeholder="Enter your email"
        />
        <span className="icon-email" title="Email" />
      </div>
      <div className="input-symbol">
        <input
          type="password"
          name="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
          placeholder="Enter your password"
        />
        <span className="icon-lock" title="Password" />
      </div>
      <button type="submit" className="btn-primary">{submitLabel}</button>
    </form>
  );
}

UserPasswordForm.propTypes = {
  submitLabel: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  setEmail: PropTypes.func.isRequired,
  password: PropTypes.string.isRequired,
  setPassword: PropTypes.func.isRequired,
};

export default withState('email', 'setEmail', '')(
  withState('password', 'setPassword', '')(UserPasswordForm),
);
