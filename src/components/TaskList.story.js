import React from 'react';
import { storiesOf, action } from '@storybook/react';

import TaskList from './TaskList';
import { TASK_STATES } from '../utils/schema';
import { buildTask } from '../utils/test-helpers';

function buildStory(tasks) {
  const onPinTask = action('onPinTask');
  const onSnoozeTask = action('onSnoozeTask');
  const onArchiveTask = action('onArchiveTask');
  const onUpdateTaskTitle = action('onUpdateTaskTitle');

  return (
    <TaskList
      {...{ tasks, onPinTask, onSnoozeTask, onArchiveTask, onUpdateTaskTitle }}
    />
  );
}

storiesOf('TaskList', module)
  .addDecorator(story => <div style={{ background: 'white' }}>{story()}</div>)
  .add('inbox', () =>
    buildStory([
      buildTask({ state: TASK_STATES.inbox, title: 'Add Asana integration', subtitle: 'hichroma/taskbox' }),
      buildTask({ state: TASK_STATES.inbox, serviceName: 'trello', title: 'Write commodity components post' }),
      buildTask({ state: TASK_STATES.inbox, serviceName: null, title: 'Pay electric bill', url: null, subtitle: null }),
    ]))
  .add('snoozed', () =>
    buildStory([
      buildTask({ state: TASK_STATES.snoozed }),
      buildTask({ state: TASK_STATES.snoozed }),
      buildTask({ state: TASK_STATES.snoozed }),
    ]))
  .add('pinned', () =>
    buildStory([
      buildTask({ state: TASK_STATES.pinned }),
      buildTask({ state: TASK_STATES.pinned }),
      buildTask({ state: TASK_STATES.pinned }),
    ]));
