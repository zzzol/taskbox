import React from 'react';
import { storiesOf, action } from '@storybook/react';

import { PureSnoozed as Snoozed } from './Snoozed';
import { TASK_STATES } from '../utils/schema';
import { buildTask } from '../utils/test-helpers';

const snoozedTasks = [
  buildTask({ state: TASK_STATES.snoozed }),
  buildTask({ state: TASK_STATES.snoozed }),
  buildTask({ state: TASK_STATES.snoozed }),
];
const onPinTask = action('onPinTask');
const onUpdateTaskTitle = action('onUpdateTaskTitle');
const events = { onPinTask, onUpdateTaskTitle };

storiesOf('Snoozed', module)
  .addDecorator(story => <div id="content-container">{story()}</div>)
  .add('loading', () => <Snoozed loading={true} />)
  .add('error', () => <Snoozed error={new Error('Foobar')} />)
  .add('no tasks', () => <Snoozed snoozedTasks={[]} {...events} />)
  .add('full', () => <Snoozed {...{ snoozedTasks, ...events }} />);
