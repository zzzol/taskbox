import React from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { compose } from 'recompose';

import TaskList from '../components/TaskList';
import { TASK_STATES } from '../utils/schema';
import {
  withOnTaskStateChanges,
  withOnUpdateTaskTitle,
  updateQueryTasklist,
} from '../utils/mutation';

const Archived = (
  { loading, error, archivedTasks, onArchiveTask, onUpdateTaskTitle }
) => {
  const events = { onArchiveTask, onUpdateTaskTitle };

  if (loading) {
    return (
      <div className="page lists-show snoozed">
        <nav>
          <h1 className="js-edit-list title-page">
            <span className="title-wrapper">Archive</span>
          </h1>
        </nav>
        <div className="content-scrollable list-items">
          <div className="wrapper-message">
            <div className="title-message">
              <span className="icon-sync" />Retrieving your tasks
            </div>
            <div className="subtitle-message">Hang on just a moment</div>
          </div>
        </div>
      </div>
    );
  }

  if (error) {
    return (
      <div className="page lists-show snoozed">
        <nav>
          <h1 className="js-edit-list title-page">
            <span className="title-wrapper">Archive</span>
          </h1>
        </nav>
        <div className="content-scrollable list-items">
          <div className="wrapper-message">
            <div className="title-message">Error</div>
            <div className="subtitle-message">{error.toString()}</div>
          </div>
        </div>
      </div>
    );
  }

  if (archivedTasks.length === 0) {
    return (
      <div className="page lists-show snoozed">
        <nav>
          <h1 className="js-edit-list title-page">
            <span className="title-wrapper">Archive</span>
          </h1>
        </nav>
        <div className="content-scrollable list-items">
          <div className="wrapper-message">
            <div className="title-message">No tasks in archive</div>
            <div className="subtitle-message">
              Your tasks that have been unassigned or closed will appear here
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="page lists-show archive">
      <nav>
        <h1 className="js-edit-list title-page">
          <span className="title-wrapper">Archive</span>
        </h1>
      </nav>
      <div className="content-scrollable list-items">
        <TaskList tasks={archivedTasks} {...events} />
      </div>
    </div>
  );
};

export { Archived as PureArchived };

const withData = graphql(
  gql`
  query ArchivedQuery {
    me {
      archivedTasks: tasks(state: ${TASK_STATES.archived}, limit: 50) {
        ...TaskListTaskFragment
      }
    }
  }
  ${TaskList.fragments.task}
`,
  {
    options: {
      forceFetch: true,
      pollInterval: 1 * 1000,
      reducer: (previousResult, action) =>
        updateQueryTasklist(
          previousResult,
          action,
          'archivedTasks',
          TASK_STATES.archived
        ),
    },
    props({ data: { loading, error, me } }) {
      if (loading) {
        return { loading };
      }
      if (error) {
        return { error };
      }
      const { archivedTasks } = me;
      return { archivedTasks };
    },
  }
);

export default compose(withOnTaskStateChanges, withOnUpdateTaskTitle, withData)(
  Archived
);
