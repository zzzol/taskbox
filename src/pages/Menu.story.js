import React from 'react';
import { storiesOf, action } from '@storybook/react';
import { MemoryRouter } from 'react-router';

import { PureMenu as Menu } from './Menu';

storiesOf('Menu', module)
  .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>
      {story()}
    </MemoryRouter>
  ))
  .add('logged out', () => {
    const serviceNames = ['trello', 'github'];
    const onLogin = action('onLogin');
    const onLogout = action('onLogout');
    const onLinkService = action('onLinkService');

    return <Menu {...{ serviceNames, onLogin, onLogout, onLinkService }} />;
  })
  .add('logged in', () => {
    const serviceNames = ['trello', 'github'];
    const user = { email: 'tom@thesnail.org', serviceNames: [] };
    const onLogin = action('onLogin');
    const onLogout = action('onLogout');
    const onLinkService = action('onLinkService');

    return (
      <Menu {...{ user, serviceNames, onLogin, onLogout, onLinkService }} />
    );
  });
