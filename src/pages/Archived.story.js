import React from 'react';
import { storiesOf, action } from '@storybook/react';

import { PureArchived as Archived } from './Archived';
import { TASK_STATES } from '../utils/schema';
import { buildTask } from '../utils/test-helpers';

const archivedTasks = [
  buildTask({ state: TASK_STATES.archived }),
  buildTask({ state: TASK_STATES.archived, serviceName: null }),
  buildTask({ state: TASK_STATES.archived, serviceName: 'trello' }),
];
const onArchiveTask = action('onArchiveTask');
const onUpdateTaskTitle = action('onUpdateTaskTitle');
const events = { onArchiveTask, onUpdateTaskTitle };

storiesOf('Archived', module)
  .addDecorator(story => <div id="content-container">{story()}</div>)
  .add('loading', () => <Archived loading={true} />)
  .add('error', () => <Archived error={new Error('Foobar')} />)
  .add('no tasks', () => <Archived archivedTasks={[]} {...events} />)
  .add('full', () => <Archived {...{ archivedTasks, ...events }} />);
