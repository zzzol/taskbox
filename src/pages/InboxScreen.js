import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { compose } from 'recompose';

import Inbox from './Inbox';
import TaskList from '../components/TaskList';
import { TASK_STATES } from '../utils/schema';
import {
  withOnTaskStateChanges,
  withOnCreateTask,
  withOnUpdateTaskTitle,
  updateQueryTasklist,
} from '../utils/mutation';

const withData = graphql(
  gql`
  query InboxQuery {
    me {
      pinnedTasks: tasks(state: ${TASK_STATES.pinned}, limit: 50) {
        ...TaskListTaskFragment
      }
      inboxTasks: tasks(state: ${TASK_STATES.inbox}, limit: 50) {
        ...TaskListTaskFragment
      }
    }
  }
  ${TaskList.fragments.task}
`,
  {
    options: {
      forceFetch: true,
      pollInterval: 1 * 1000,
      reducer: (previousResult, action) =>
        updateQueryTasklist(
          updateQueryTasklist(
            previousResult,
            action,
            'pinnedTasks',
            TASK_STATES.pinned
          ),
          action,
          'inboxTasks',
          TASK_STATES.inbox
        ),
    },
    props({ data: { loading, error, me } }) {
      if (loading) {
        return { loading };
      }
      if (error) {
        return { error };
      }
      const { pinnedTasks, inboxTasks } = me;
      return { pinnedTasks, inboxTasks };
    },
  }
);

export default compose(
  withOnCreateTask,
  withOnTaskStateChanges,
  withOnUpdateTaskTitle,
  withData
)(Inbox);
