import PropTypes from 'prop-types';
import React from 'react';
import { withState } from 'recompose';

import Login from './Login';
import Signup from './Signup';

function Auth({ onLogin, signingUp, setSigningUp }) {
  return (
    <div id="content-container" className="page auth">
      {/*  XXX: mobile menu? */}
      <nav />
      <div className="content-scrollable">
        {signingUp ? <Signup onLogin={onLogin} /> : <Login onLogin={onLogin} />}
        {signingUp
          ? <a onClick={() => setSigningUp(false)} className="link-auth-alt">
              Sign in
            </a>
          : <a onClick={() => setSigningUp(true)} className="link-auth-alt">
              Sign up
            </a>}
      </div>
    </div>
  );
}

Auth.propTypes = {
  onLogin: PropTypes.func.isRequired,
  signingUp: PropTypes.bool.isRequired,
  setSigningUp: PropTypes.func.isRequired,
};

export default withState('signingUp', 'setSigningUp', false)(Auth);
