import React from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { NavLink } from 'react-router-dom';

import LoginButton from '../components/LoginButton';

const Menu = ({
  loading,
  user,
  serviceNames,
  onLogin,
  onLogout,
  onLinkService
}) => (
  <section id="menu">
    {loading ||
      <LoginButton
        user={user}
        serviceNames={serviceNames}
        onLogin={onLogin}
        onLogout={onLogout}
        onLinkService={onLinkService}
      />}
    <div className="list-todos">
      <NavLink to="/" className="list-todo" activeClassName="active" exact>
        Taskbox
      </NavLink>
      <NavLink to="/snoozed" className="list-todo" activeClassName="active">
        Snoozed
      </NavLink>
      <NavLink to="/archived" className="list-todo" activeClassName="active">
        Archive
      </NavLink>
    </div>
  </section>
);

export { Menu as PureMenu };

export default graphql(
  gql`
  query MenuQuery {
    me {
      ...LoginButtonFragment
    }
    serviceNames
  }
  ${LoginButton.fragments.user}
`,
  {
    props({ data: { loading, error, me, serviceNames } }) {
      // XXX: Hack, find a proper place to identify users
      if (me) {
        console.log('Logged In');
        window.analytics &&
          window.analytics.identify(me.email, {
            // The following trait will show the user's email in the user list in
            // Fullstory
            displayName: me.email,
            email: me.email
          });
      }

      return { loading, user: me, serviceNames: serviceNames || [] };
    }
  }
)(Menu);
