import PropTypes from 'prop-types';
import React from 'react';
import { Route } from 'react-router-dom';

import Menu from './Menu';
import InboxScreen from './InboxScreen';
import Snoozed from './Snoozed';
import Archived from './Archived';
import Auth from './Auth';

const App = ({ loggedIn, onLogin, onLogout, onLinkService }) => (
  <div id="container" className="menuOpen">
    <Menu onLogin={onLogin} onLogout={onLogout} onLinkService={onLinkService} />
    {loggedIn
      ? <div id="content-container">
          <Route exactly path="/" component={InboxScreen} />
          <Route exactly path="/snoozed" component={Snoozed} />
          <Route exactly path="/archived" component={Archived} />
        </div>
      : <Auth onLogin={onLogin} />}
  </div>
);

App.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  onLogin: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
  onLinkService: PropTypes.func.isRequired
};

export default App;
