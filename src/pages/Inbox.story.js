import React from 'react';
import { storiesOf, action } from '@storybook/react';

import Inbox from './Inbox';
import { TASK_STATES } from '../utils/schema';
import { buildTask } from '../utils/test-helpers';

const pinnedTasks = [
  buildTask({ state: TASK_STATES.pinned }),
  buildTask({ state: TASK_STATES.pinned }),
  buildTask({ state: TASK_STATES.pinned }),
];
const inboxTasks = [
  buildTask({ state: TASK_STATES.inbox }),
  buildTask({ state: TASK_STATES.inbox }),
  buildTask({ state: TASK_STATES.inbox }),
];
const onSnoozeTask = action('onSnoozeTask');
const onPinTask = action('onPinTask');
const onCreateTask = action('onCreateTask');
const onUpdateTaskTitle = action('onUpdateTaskTitle');
const events = { onSnoozeTask, onPinTask, onCreateTask, onUpdateTaskTitle };

storiesOf('Inbox', module)
  .addDecorator(story => <div id="content-container">{story()}</div>)
  .add('loading', () => <Inbox loading={true} />)
  .add('error', () => <Inbox error={new Error('Foobar')} />)
  .add('no tasks', () => <Inbox pinnedTasks={[]} inboxTasks={[]} {...events} />)
  .add('no pinned tasks', () => (
    <Inbox pinnedTasks={[]} {...{ inboxTasks, ...events }} />
  ))
  .add('no inbox tasks', () => (
    <Inbox
      inboxTasks={[]}
      {...{
        pinnedTasks,
        ...events,
      }}
    />
  ))
  .add('full', () => (
    <Inbox
      {...{
        pinnedTasks,
        inboxTasks,
        ...events,
      }}
    />
  ));
