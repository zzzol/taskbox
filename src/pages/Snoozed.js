import React from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { compose } from 'recompose';

import TaskList from '../components/TaskList';
import { TASK_STATES } from '../utils/schema';
import {
  withOnTaskStateChanges,
  withOnUpdateTaskTitle,
  updateQueryTasklist,
} from '../utils/mutation';

const Snoozed = (
  {
    loading,
    error,
    snoozedTasks,
    onPinTask,
    onArchiveTask,
    onSnoozeTask,
    onUpdateTaskTitle,
  }
) => {
  const events = { onPinTask, onArchiveTask, onSnoozeTask, onUpdateTaskTitle };

  if (loading) {
    return (
      <div className="page lists-show snoozed">
        <nav>
          <h1 className="js-edit-list title-page">
            <span className="title-wrapper">Snoozed</span>
          </h1>
        </nav>
        <div className="content-scrollable list-items">
          <div className="wrapper-message">
            <div className="title-message">
              <span className="icon-sync" />Retrieving your tasks
            </div>
            <div className="subtitle-message">Hang on just a moment</div>
          </div>
        </div>
      </div>
    );
  }

  if (error) {
    return (
      <div className="page lists-show snoozed">
        <nav>
          <h1 className="js-edit-list title-page">
            <span className="title-wrapper">Snoozed</span>
          </h1>
        </nav>
        <div className="content-scrollable list-items">
          <div className="wrapper-message">
            <div className="title-message">Error</div>
            <div className="subtitle-message">{error.toString()}</div>
          </div>
        </div>
      </div>
    );
  }

  if (snoozedTasks.length === 0) {
    return (
      <div className="page lists-show snoozed">
        <nav>
          <h1 className="js-edit-list title-page">
            <span className="title-wrapper">Snoozed</span>
          </h1>
        </nav>
        <div className="content-scrollable list-items">
          <div className="wrapper-message">
            <div className="title-message">No snoozed tasks</div>
            <div className="subtitle-message">
              You haven't snoozed any tasks yet
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="page lists-show snoozed">
      <nav>
        <h1 className="js-edit-list title-page">
          <span className="title-wrapper">Snoozed</span>
        </h1>
      </nav>
      <div className="content-scrollable list-items">
        <TaskList tasks={snoozedTasks} {...events} />
      </div>
    </div>
  );
};

export { Snoozed as PureSnoozed };

const withData = graphql(
  gql`
  query SnoozedQuery {
    me {
      snoozedTasks: tasks(state: ${TASK_STATES.snoozed}, limit: 50) {
        ...TaskListTaskFragment
      }
    }
  }
  ${TaskList.fragments.task}
`,
  {
    options: {
      forceFetch: true,
      pollInterval: 1 * 1000,
      reducer: (previousResult, action) =>
        updateQueryTasklist(
          previousResult,
          action,
          'snoozedTasks',
          TASK_STATES.snoozed
        ),
    },
    props({ data: { loading, error, me } }) {
      if (loading) {
        return { loading };
      }
      if (error) {
        return { error };
      }
      const { snoozedTasks } = me;
      return { snoozedTasks };
    },
  }
);

export default compose(withOnTaskStateChanges, withOnUpdateTaskTitle, withData)(
  Snoozed
);
