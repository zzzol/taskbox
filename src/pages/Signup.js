import PropTypes from 'prop-types';
import React from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import UserPasswordForm from '../components/UserPasswordForm';

function Signup({ onSignup }) {
  return (
    <div className="wrapper-auth">
      <h1 className="title-auth">Join Taskbox</h1>
      <p className="subtitle-auth">
        Join now!
      </p>
      <UserPasswordForm submitLabel="Signup" onSubmit={onSignup} />
    </div>
  );
}

Signup.propTypes = {
  onSignup: PropTypes.func.isRequired,
};

const SignupWithMutation = graphql(
  gql`
  mutation SignupMutation($email: String!, $password: String!) {
    createUser(input: {
      email: $email,
      password: $password
    }) {
      id
    }
  }
`,
  {
    props: ({ ownProps: { onLogin }, mutate }) => ({
      onSignup: (email, password) =>
        mutate({ variables: { email, password } }).then(() =>
          onLogin(email, password)),
    }),
  },
)(Signup);

SignupWithMutation.props = {
  onLogin: PropTypes.func.isRequired,
};

export default SignupWithMutation;
