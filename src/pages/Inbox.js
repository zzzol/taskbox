import React from 'react';

import TaskList from '../components/TaskList';
import TaskCreator from '../components/TaskCreator';

const Inbox = (
  {
    loading,
    error,
    inboxTasks,
    pinnedTasks,
    onCreateTask,
    onSnoozeTask,
    onPinTask,
    onArchiveTask,
    onUpdateTaskTitle,
  },
) => {
  let messageTitle;
  let messageSubtitle;
  let creator;
  let lists = [];

  const events = {
    onSnoozeTask,
    onPinTask,
    onArchiveTask,
    onUpdateTaskTitle,
  };

  if (loading) {
    messageTitle = <span><span className="icon-sync" />Getting your tasks</span>;
    messageSubtitle = 'Retrieving the tasks assigned to you from your services'
  } else if (error) {
    messageTitle = 'Error';
    messageSubtitle = error.toString();
  } else {
    creator = <TaskCreator onCreateTask={onCreateTask} />;

    if (pinnedTasks.length === 0 && inboxTasks.length === 0) {
      messageTitle = 'No Tasks';
      messageSubtitle = 'You don\'t have any open tasks assigned to you yet. '
    } else {
      messageTitle = 'Taskbox';

      if (pinnedTasks.length > 0) {
        lists = lists.concat([
          <h3 className="list-heading" key="pinned-title">Important</h3>,
          <TaskList key="pinned-tasks" tasks={pinnedTasks} {...events} />,
        ]);
      }

      if (inboxTasks.length > 0) {
        lists = lists.concat([
          <h3 className="list-heading" key="inbox-title">Tasks</h3>,
          <TaskList key="inbox-tasks" tasks={inboxTasks} {...events} />,
        ]);
      }
    }
  }

  return (
    <div className="page lists-show">
      <nav>
        <h1 className="js-edit-list title-page">
          <span className="title-wrapper">Taskbox</span>
        </h1>
        {creator}
      </nav>
      <div className="content-scrollable list-items">
        {lists.length > 0 ? lists
          : <div className="wrapper-message">
              <div className="title-message">{messageTitle}</div>
              <div className="subtitle-message">{messageSubtitle}</div>
            </div>
        }
      </div>
    </div>
  );
};

export default Inbox;
