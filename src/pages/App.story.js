import React from 'react';
import { storiesOf, action } from '@storybook/react';
import { MemoryRouter } from 'react-router';
import gql from 'graphql-tag';
import { addTypenameToDocument } from 'apollo-client';

import App from './App';
import TaskList from '../components/TaskList';
import LoginButton from '../components/LoginButton';
import { TASK_STATES } from '../utils/schema';
import { MockedProvider, buildTask } from '../utils/test-helpers';

const homeQuery = gql`
  query InboxQuery {
    me {
      pinnedTasks: tasks(state: ${TASK_STATES.pinned}, limit: 50) {
        ...TaskListTaskFragment
      }
      inboxTasks: tasks(state: ${TASK_STATES.inbox}, limit: 50) {
        ...TaskListTaskFragment
      }
    }
  }
  ${TaskList.fragments.task}
`;

const homeData = {
  me: {
    pinnedTasks: [
      buildTask({ state: TASK_STATES.pinned }),
      buildTask({ state: TASK_STATES.pinned }),
      buildTask({ state: TASK_STATES.pinned })
    ],
    inboxTasks: [
      buildTask({ state: TASK_STATES.inbox }),
      buildTask({ state: TASK_STATES.inbox }),
      buildTask({ state: TASK_STATES.inbox })
    ],
    __typename: 'User'
  }
};

const snoozedQuery = gql`
  query SnoozedQuery {
    me {
      snoozedTasks: tasks(state: ${TASK_STATES.snoozed}, limit: 50) {
        ...TaskListTaskFragment
      }
    }
  }
  ${TaskList.fragments.task}
`;

const snoozedData = {
  me: {
    snoozedTasks: [
      buildTask({ state: TASK_STATES.snoozed }),
      buildTask({ state: TASK_STATES.snoozed }),
      buildTask({ state: TASK_STATES.snoozed })
    ],
    __typename: 'User'
  }
};

const menuQuery = gql`
  query MenuQuery {
    me {
      ...LoginButtonFragment
    }
    serviceNames
  }
  ${LoginButton.fragments.user}
`;

const menuData = {
  me: {
    email: 'test@test.com',
    serviceNames: [],
    __typename: 'User'
  },
  serviceNames: []
};

const mocks = [
  {
    request: { query: addTypenameToDocument(homeQuery) },
    result: { data: homeData },
    newData: () => homeData
  },
  {
    request: { query: addTypenameToDocument(snoozedQuery) },
    result: { data: snoozedData },
    newData: () => snoozedData
  },
  {
    request: { query: addTypenameToDocument(menuQuery) },
    result: { data: menuData }
  }
];

function buildStory(url) {
  return (
    <div style={{ background: 'white' }}>
      <MockedProvider mocks={mocks}>
        <MemoryRouter initialEntries={[url]}>
          <App
            loggedIn={true}
            onLogin={action('onLogin')}
            onLogout={action('onLogout')}
            onLinkService={action('onLinkService')}
          />
        </MemoryRouter>
      </MockedProvider>
    </div>
  );
}

storiesOf('App', module)
  .add('homepage', () => buildStory('/'))
  .add('snoozed', () => buildStory('/snoozed'));
