// import React from 'react';
// import { storiesOf } from '@storybook/react';
// import gql from 'graphql-tag';
// import { addTypenameToDocument } from 'apollo-client';

// import InboxScreen from './InboxScreen';
// import TaskList from '../components/TaskList';
// import { TASK_STATES } from '../utils/schema';
// import { MockedProvider, buildTask } from '../utils/test-helpers';

// const query = addTypenameToDocument(
//   gql`
//   query InboxQuery {
//     me {
//       pinnedTasks: tasks(state: ${TASK_STATES.pinned}, limit: 50) {
//         ...TaskListTaskFragment
//       }
//       inboxTasks: tasks(state: ${TASK_STATES.inbox}, limit: 50) {
//         ...TaskListTaskFragment
//       }
//     }
//   }
//   ${TaskList.fragments.task}
// `
// );

// const pinnedTasks = [
//   buildTask({ state: TASK_STATES.pinned }),
//   buildTask({ state: TASK_STATES.pinned }),
//   buildTask({ state: TASK_STATES.pinned }),
// ];

// const inboxTasks = [
//   buildTask({ state: TASK_STATES.inbox }),
//   buildTask({ state: TASK_STATES.inbox }),
//   buildTask({ state: TASK_STATES.inbox }),
// ];

// const buildMocks = ({ pinnedTasks, inboxTasks }) => {
//   const data = {
//     me: {
//       pinnedTasks,
//       inboxTasks,
//       __typename: 'User',
//     },
//   };
//   return [
//     {
//       request: { query },
//       result: { data },
//       newData: () => data,
//       delay: 1000,
//     },
//   ];
// };

// storiesOf('InboxScreen', module)
//   .add('no data', () => (
//     <MockedProvider mocks={buildMocks({ pinnedTasks: [], inboxTasks: [] })}>
//       <InboxScreen />
//     </MockedProvider>
//   ))
//   .add('error', () => {
//     const mock = {
//       request: { query },
//       error: new Error('Failed to fetch data'),
//       delay: 1000,
//     };
//     return (
//       <MockedProvider mocks={[mock]}>
//         <InboxScreen />
//       </MockedProvider>
//     );
//   })
//   .add('only inbox', () => (
//     <MockedProvider mocks={buildMocks({ pinnedTasks: [], inboxTasks })}>
//       <InboxScreen />
//     </MockedProvider>
//   ))
//   .add('only pinned', () => (
//     <MockedProvider mocks={buildMocks({ pinnedTasks, inboxTasks: [] })}>
//       <InboxScreen />
//     </MockedProvider>
//   ))
//   .add('full data', () => (
//     <MockedProvider mocks={buildMocks({ pinnedTasks, inboxTasks })}>
//       <InboxScreen />
//     </MockedProvider>
//   ));
