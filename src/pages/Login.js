import PropTypes from 'prop-types';
import React from 'react';
import UserPasswordForm from '../components/UserPasswordForm';

function Login({ onLogin }) {
  return (
    <div className="wrapper-auth">
      <h1 className="title-auth">Login to Taskbox</h1>
      <p className="subtitle-auth">
        Seems like a good plan
      </p>
      <UserPasswordForm submitLabel="Login" onSubmit={onLogin} />
    </div>
  );
}

Login.propTypes = {
  onLogin: PropTypes.func.isRequired,
};

export default Login;
